/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
За допомогою вункції setInterval ми виставляємо інтервал з яким певна дія буде виконуватись, а setTimeout ми виставляємо затримку між виконанням задачі
2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
ні, існують багато різних чиників, які можуть завадити точнумо виконанню, хоча ці методи забезпечують певний рівень точност
3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
для setInterval за допомогою clearInterval, а setTimeout clearTimeout для призупинення

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
Новий текст повинен вказувати, що операція виконана успішно.
*/
const div = document.querySelector('div')
const button = document.querySelector('button')

button.addEventListener('click', () => {
    setTimeout(()=>{
        div.innerText = 'Операція виконана успішно'
    },3000)
})

/*Практичне завдання 2:

Реалізуйте таймер зворотного відліку, використовуючи setInterval.
При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/
const timer = document.querySelector('.timer')
let tick = setInterval(() => {
    let newTimer = timer.textContent -1;
    timer.textContent = newTimer;
},1000)

setTimeout(()=>{
    clearInterval(tick)
    timer.textContent = "Зворотній відлік завершено"
},10000)
